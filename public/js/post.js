// $(document).ready(function () {
//     $('#createPost').submit(function (event) {
//         event.preventDefault();
//         ajaxcreatePost();
//     })
//     function ajaxcreatePost() {

//         var formdata = {
//             title: $('#post-title').val(),
//             description: $('#description').val(),
//            created_at: $('#date').val()
//         }
//         console.log(formdata)
//         $.ajax({
//             type: 'POST',
//             contentType: 'application/json',
//             url: window.location + "/",
//             data: JSON.stringify(formdata),
//             dataType: 'json',
//             success:function(data){
//                 $('#response pre').html(data);
//             },
//             error: function (xhr, status, error) {
//                 var errorMessage = xhr.status + ': ' + xhr.statusText
//                 alert('Error - ' + errorMessage);
//             }

//         })

//     }




// })

$(document).ready(function () {

    $('#createPost').submit(function (event) {
        event.preventDefault();
        ajaxPost();

    })

    function ajaxPost() {
        var formdata = {
            posttitle: $('#post-title').val(),
            description: $('#description').val(),
            createat: $('#date').val()
        }
        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: window.location + "created",
            data: JSON.stringify(formdata),
            dataType: 'json',
            success: function (data, status) {
                // alert(data.message)
                $("#alert-success").css({ "background-color": "#7bea7b", "height": "50px", "font- size": "200 %", "text-align": "center" }).text(data.message);
            }
        })
        resetData();
    }
    function resetData() {
        $('#post-title').val(" "),
            $('#description').val(" "),
            $('#date').val(" ")

    }

})


$(document).ready(function () {

    $('#updatePost').submit(function (event) {
        event.preventDefault();
        ajaxUpdate();

    })

    function ajaxUpdate() {
      var  id= $('#ID').val();
        var formdata = {
            
            posttitle: $("#update-post-title").val(),
            description: $('#update-description').val(),
            createat: $('#update-date').val()
        }
        $.ajax({
            type: 'PUT',
            contentType: 'application/json',
            url: window.location + `update/${id}`,
            data: JSON.stringify(formdata),
            dataType: 'json',
            success: function (data, status) {
                // alert(data.message)
               
                    $("#update-alert-success").css({ "background-color": "#7bea7b", "height": "50px", "font- size": "200 %", "text-align": "center" }).text(data.message); 
            },
        })
        resetData();
    }
    function resetData() {
        $('#ID').val(""),
            $('#update-post-title').val(""),
            $('#update-description').val(""),
            $('#update-date').val("")

    }

})





$(document).ready(function () {
    $('#delete-page').submit(function (event) {
        event.preventDefault();
        ajaxPost();
    })
    function ajaxPost() {
        var deleteid = {
            id: $('#ID').val()
        }
        $.ajax({
            type: 'DELETE',
            contentType: 'application/json',
            url: window.location + `deletepost`,
            data: JSON.stringify(deleteid),
            dataType: 'json',
            success: function (data, status) {
                // alert(data.message)
                $("#delete-alert-success").css({ "background-color": "#7bea7b", "height": "50px", "font- size": "200 %", "text-align": "center" }).text(data.message);
            }
        })
        resetData();
    }
    function resetData() {

        $('#ID').val("")
    }
})


$(document).ready(function () {
    ajaxGet();
    $('#allpost').click(function (event) {
        event.preventDefault();
        ajaxGet();
    })
    function ajaxGet() {
        $.ajax({
            type: "GET",
            url: window.location + "posts",
            success: function (result) {

                $('#get-data ul').empty();
                var users = ' ';
                $.each(result.data, function (i, item) {
                    users += '<tr>';
                    users += '<td>' + item.ID + '</td>';
                    users += '<td>' + item.POSTTITLE + '</td>';
                    users += '<td>' + item.POSTDESCRIPTION + '</td>';
                    users += '<td>' + item.CREATEAT + '</td>';
                });
                $('#get-data').append(users);

            }
        })
    }


})
