const express= require('express')
const router= express.Router();
const controller= require('../controller/post.controller');

router.route('/created').post(controller.createpost);
router.route('/update/:id').put(controller.updatepost);
router.route('/deletepost').delete(controller.deletePost);
router.route('/posts').get(controller.allpost);


module.exports = router;