const express = require('express');
const app=express();
const dotenv=require('dotenv');
dotenv.config()
const path=require('path');
const bodyparser = require('body-parser');
const routes=require('./routes/post.routes')

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: false }))
app.use(express.static(path.join(__dirname, './public')));

app.use('/', routes);



app.listen(process.env.PORT || 3000, () =>{
console.log('listening on port '+ process.env.PORT);
})