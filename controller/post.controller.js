const knex = require('../models/postdb')
const postData = {

    async createpost(req, res) {

        try {
            const data = [
                {
                    POSTTITLE: req.body.posttitle,
                    POSTDESCRIPTION: req.body.description,
                    CREATEAT: req.body.createat,
                }
            ]

            const created = await knex('POSTS_DATA').select('POSTTITLE', 'POSTDESCRIPTION', 'CREATEAT').insert(data);
            if (created.length > 0) {
                return res.json({
                    success: true,
                    status:200,
                    message: "created"
                })
            }
            res.status(401).json({
                success: false,
                message: 'Post Does not Created'
            })
        } catch (error) {
            return res.status(500).json({
                success: false,
                message: error.message
            })
        }
    },
    // update Post API
    async updatepost(req, res) {
        try {
            const updatepostid = req.params.id;  
            const checkupdateid = await knex('POSTS_DATA').select('ID').where({ ID: updatepostid });
            
            if (checkupdateid.length!=0) {
                const updatedata = await knex('POSTS_DATA').update({ POSTTITLE: req.body.posttitle, POSTDESCRIPTION: req.body.description, CREATEAT: req.body.createat }).where({ ID: updatepostid });
                
                return res.json({
                    success: true,
                    message: 'Successfully updated'
                })
            }
            res.json({
                success: false,
                message: "Id doesnot Exist"
            })

        } catch (error) {
            return res.status(500).json({
                success: false,
                message: error.message
            })
        }
    },
    async deletePost(req, res) {
        try {
            const checkdeleteid = await knex('POSTS_DATA').select('ID').where({ ID: req.body.id })
            if (checkdeleteid != 0) {
                await knex("POSTS_DATA").delete().where({ ID: req.body.id });
                return res.json({
                    success: true,
                    message: 'Successfully deleted'
                })
            }
            res.json({
                success: false,
                message: "Id doesnot Exist"
            })
        } catch (error) {
            return res.status(500).json({
                success: false,
                message: error.message
            })
        }
    },
    async allpost(req, res) {
        try {
            const allpostdata = await knex('POSTS_DATA').select('*');
            if (allpostdata != 0) {
                return res.json({
                    success: true,
                    data:allpostdata
                })
            }
            res.json({
                success: false,
                message:"No Post data found"
            })
        } catch (error) {
            return res.status(500).json({
                success: false,
                message: error.message
            })
        }
    }
}

module.exports = postData